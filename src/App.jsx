import { BrowserRouter, Routes, Route } from "react-router-dom";
import 'bulma/css/bulma.min.css';
import './App.css';

import Home from './pages/home';
import Connect from './pages/connect';
import Register from './pages/register';
import {Logout} from './pages/logout' 
import Products from './pages/products';
import ProductDetails from './pages/productDetails';
import {ProductParam, ProductAdd, ProductDelete, ProductEdit} from './pages/productsParam';
import {UserParam, UserDelete, UserEdit} from './pages/usersParam';
import Cart from './pages/cart'
import { OrderList, OrderDelete, OrderEdit } from "./pages/ordersList";
import { OrderDetail } from "./pages/orderDetails";

import Nav from './components/nav';

import { EcommerceContext } from './contexte/ecommerce.js'
import { useState } from 'react'

function App() {
  const [token, setToken] = useState("");
  const [mail, setMail] = useState("");
  const [admin, setAdmin] = useState(false);
  const [cartProducts, setCartProducts] = useState([])
  return (
    <EcommerceContext.Provider value={{ token, setToken, admin, setAdmin, cartProducts, setCartProducts, mail, setMail }}>
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/:id" element={<ProductDetails  />} />
          <Route path="/login" element={<Connect />} />
          <Route path="/register" element={<Register />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/productsParam" element={<ProductParam />} />
          <Route path='/productsParam/add' element={< ProductAdd />}></Route>
          <Route path='/productsParam/edit/:id' element={< ProductEdit />}></Route>
          <Route path='/productsParam/delete/:id' element={< ProductDelete />}></Route>
          <Route path="/usersParam" element={<UserParam />} />
          <Route path='/usersParam/edit/:id' element={< UserEdit />}></Route>
          <Route path='/usersParam/delete/:id' element={< UserDelete />}></Route>
          <Route path='/cart' element={<Cart />}></Route>
          <Route path='/orders' element={<OrderList />}></Route>
          <Route path='/orders/:id' element={<OrderDetail />}></Route>
          <Route path='/orders/delete/:id' element={<OrderDelete />}></Route>
          <Route path='/orders/edit/:id' element={<OrderEdit />}></Route>

        </Routes>
      </BrowserRouter>
    </EcommerceContext.Provider>
  )
}

export default App;
