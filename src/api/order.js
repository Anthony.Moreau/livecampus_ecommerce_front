function createOrder(itemList, token) {
    return new Promise((resolve, reject) => {

        fetch("http://localhost:3000/order", {
            method: "POST",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ articles: itemList })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

function getAllOrdersByUser(mail) {
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:3000/order/user/${mail}`, {
            method: "GET",
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

function getAllOrders() {
    return new Promise((resolve, reject) => {
        fetch(`http://localhost:3000/order`, {
            method: "GET",
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

function getOrder(id) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/order/${id}`, {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
function getOrderLines(id) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/order/details/${id}`, {
            method: "GET"
        }).then(res => res.json())
            .then(data => {
                // console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

export function editOrder(id, statut, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/order/${id}`, {
            method: "PUT",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            },
            body: JSON.stringify({ statut })
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}
export function deleteOrder(id, token) {
    return new Promise((resolve, reject) => {

        fetch(`http://localhost:3000/order/${id}`, {
            method: "DELETE",
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-type': "application/json"
            }
        }).then(res => res.json())
            .then(data => {
                console.log(data);
                return resolve(data)
            })
            .catch(err => reject(err))
    })
}

export { createOrder, getAllOrdersByUser, getAllOrders, getOrder, getOrderLines }
