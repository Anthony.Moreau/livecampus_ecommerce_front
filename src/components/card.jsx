import React from 'react';
import { Link } from 'react-router-dom';
import { EcommerceContext } from "../contexte/ecommerce";
import { useContext } from "react";

function Card({id, photo, nom, description, prix}) {
    const { token } = useContext(EcommerceContext);
    return (
        <div class="card">
            <div class="card-image">
                <figure>
                    <img src={photo} alt={nom}></img>
                </figure>
            </div>
            <div class="card-content">
                <div class="media">
                    <div class="media-content">
                        <p class="title is-4">{nom}</p>
                    </div>
                </div>
                <div class="media">
                    <div class="media-content">
                        <p class="title is-4">{prix} €</p>
                    </div>
                </div>

                <div class="content">
                    {description}
                </div>
                <div class="content">
                    <Link to={`/products/${id}`}>
                        <button class="button is-dark">En savoir plus</button>
                    </Link>
                </div>
            </div>
        </div>
    );
}
export default Card;