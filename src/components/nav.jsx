import React from 'react';
import { useContext, useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import { EcommerceContext } from "../contexte/ecommerce";
import { Cart } from 'react-bootstrap-icons'

function Nav(props) {
    const { token, admin, cartProducts } = useContext(EcommerceContext);
    const [prixTotal, setPrixTotal] = useState(0)
    useEffect(() => {
        let total = 0
        cartProducts.map( item => (
            total += item.quantity * item.product.prix
        ))
        setPrixTotal(total)
    }, [cartProducts])

    console.log({admin})
    return (
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <h1 className="navbar-item title is-4">
                    Projet E-Commerce
                </h1>
            </div>

            <div class="navbar-brand">

                <Link role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </Link>
            </div>

            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                    <Link class="navbar-item" to="/">
                        Accueil
                    </Link>

                    <Link class="navbar-item" to="/products">
                        Les Produits
                    </Link>

                    {(admin === true) ?
                        <>
                            <div class="navbar-item has-dropdown is-hoverable">
                                <Link class="navbar-link">
                                    Admin
                                </Link>

                                <div class="navbar-dropdown">
                                    <Link class="navbar-item" to='/usersParam'>
                                        Liste des utilisateurs
                                    </Link>
                                    <Link class="navbar-item" to='/productsParam'>
                                        Liste des produits
                                    </Link>
                                    <Link class="navbar-item" to="/orders">
                                        Liste des commandes
                                    </Link>
                                </div>
                            </div>
                        </>
                        :
                        <>
                            
                            <div class="navbar-item has-dropdown is-hoverable">
                                <Link class="navbar-link">
                                    More
                                </Link>
                                <div class="navbar-dropdown">
                                    {(token !== "") ?
                                        <>
                                            <Link class="navbar-item" to="/orders">
                                                Mes Commandes
                                            </Link>
                                        </>
                                        :
                                        <>
                                        </>
                                    }
                                    <hr class="navbar-divider"></hr>
                                        <Link class="navbar-item">
                                            Raporter un bug
                                        </Link>
                                </div>
                            </div>
                        </>
                    }
                </div>

                <div class="navbar-end">
                    <div className="navbar-item">
                        <Link to={'/cart'}>
                        <div className="control">
                            <div className="tags has-addons ">
                                <span className="tag is-dark is-large"><Cart /></span>
                                <span className="tag is-light is-large">{prixTotal} €</span>
                            </div>
                        </div>
                        </Link>
                    </div>

                    <div class="navbar-item">
                    {(token !== "") ?
                        <>
                            <div class="buttons">
                                <Link class="button is-danger" to="/logout">
                                    <strong>Logout</strong>
                                </Link>
                            </div>
                        </>
                        :
                        <>
                            <div class="buttons">
                                <Link class="button is-dark" to="/register">
                                    <strong>S'inscrire</strong>
                                </Link>
                                <Link class="button is-light" to="/login">
                                    Se connecter
                                </Link>
                            </div>
                        </>
                    }
                    </div>
                </div>
            </div>
        </nav>
    )
}
export default Nav;