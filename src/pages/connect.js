import { useContext, useState } from "react"
import { useNavigate } from "react-router-dom";
import { connectUser } from "../api/user"
import { EcommerceContext } from "../contexte/ecommerce";

function Connect() {
    const [email, setEmail] = useState("")
    const [pass, setPass] = useState("")
    const { setToken, setAdmin, setMail } = useContext(EcommerceContext);
    const navigate = useNavigate()
    const submitForm = (e) => {
        e.preventDefault();
        connectUser(email, pass).then(data => {
            if(typeof data.token !== 'string'){
                alert(data.mess)
            } else {
                console.log(data.admin)
                console.log(data.email)
                setToken(data.token)
                setAdmin(data.admin === 1)
                setMail(data.email)
                navigate("/")
            }
        })
        console.log(email, pass)
    }
    return (
        <div class="connectpage">
            <h1>Se Connecter</h1>
            <form>
                <div class="field">
                    <p class="control has-icons-left has-icons-right">
                        <input class="input" type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)}></input>
                            <span class="icon is-small is-left">
                                <i class="fas fa-envelope"></i>
                            </span>
                            <span class="icon is-small is-right">
                                <i class="fas fa-check"></i>
                            </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control has-icons-left">
                        <input class="input" type="password" placeholder="Password" value={pass} onChange={(e) => setPass(e.target.value)}></input>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control">
                        <button class="button is-dark" onClick={submitForm}>
                            Login
                        </button>
                    </p>
                </div>
            </form>
        </div>
    );
}
export default Connect