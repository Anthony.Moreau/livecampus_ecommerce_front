import { useContext, useState, useEffect } from "react"
import { useNavigate, useParams } from "react-router-dom";
import { getProduct } from "../api/products"
import { EcommerceContext } from "../contexte/ecommerce";

import Card from '../components/card';

function ProductDetail() {
  const { id } = useParams();
  const { cartProducts, setCartProducts } = useContext(EcommerceContext);
  const [product, setProducts] = useState({})
  const [quantity, setQuantity] = useState(1)
  useEffect(() => {
      getProduct(id).then(data => {
          setProducts(data)
      })
  }, [])
  const submitForm = (e) => {
      e.preventDefault();
      console.log(cartProducts);
      const index = cartProducts.findIndex(elem => elem.product.id == product.id)
      if(index == -1){
          console.log(index);
          console.log('ajout produit au panier');
          setCartProducts([...cartProducts, { product, quantity: (+quantity) }])
      } else {
          console.log('modif produit au panier');
          cartProducts[index].quantity += (+quantity)
          setCartProducts( [...cartProducts])
      }
      console.log(product, quantity)
  }
  return (
      <div className='app'>
          <div className='product-detail'>
              <div>
                  <div className='product-img'>
                      <img src={product.photo} />
                  </div>
                  <div className='product-detail'>
                      <div className='product-info'>
                          <p className="title is-4 is-dark" title={product.nom}>{product.nom}</p>
                          <br />
                          <p className="subtitle is-6 box button is-light" style={{ fontWeight: 'bold' }}>{product.prix} €</p>
                          <br />
                          <div>
                              {product.description}
                          </div>
                          <br />
                      </div>
                      <form className='product-buy' action="/" method='post' onSubmit={submitForm}>
                          <p className='title is-5'>Quantité : </p>
                          <input className="input" type="number" value={quantity} onChange={(e) => setQuantity(e.target.value <= 0 ? 1 : e.target.value)} />
                          <br /><br />
                          <button className="button is-dark" style={{ width: '100%' }}>Ajouter au panier</button>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  );
}
export default ProductDetail;
