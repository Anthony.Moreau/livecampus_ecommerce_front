import { useContext, useState } from "react"
import { useNavigate } from "react-router-dom";
import { registerUser } from "../api/user"
import { EcommerceContext } from "../contexte/ecommerce";

function Register() {

    const [email, setEmail] = useState("")
    const [pass, setPass] = useState("")
    const [confirm, setConfirm] = useState("")
    const { setToken, setAdmin } = useContext(EcommerceContext);
    const navigate = useNavigate()

    const submitForm = (e) => {
        e.preventDefault()
        if (pass !== confirm) {
            alert("Les mots de passes sont différents")
        }
        else {
            registerUser(email, pass).then(data => {
                if(data.mess){
                alert(data.mess)
                }
                else{
                    setToken(data.token)
                    setAdmin(data.admin)
                    navigate('/')
                }
            
        })
        }
    }
    return (
        <div class="connectpage">
            <h1>S'inscrire</h1>
            <form>
                <div class="field">
                    <p class="control has-icons-left has-icons-right">
                        <input class="input" type="email" placeholder="Email" value = {email} onChange={(e) => setEmail(e.target.value)}></input>
                            <span class="icon is-small is-left">
                                <i class="fas fa-envelope"></i>
                            </span>
                            <span class="icon is-small is-right">
                                <i class="fas fa-check"></i>
                            </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control has-icons-left">
                        <input class="input" type="password" placeholder="Password" value = {pass} onChange={(e) => setPass(e.target.value)}></input>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control has-icons-left">
                        <input class="input" type="password" placeholder="Password" value = {confirm} onChange={(e) => setConfirm(e.target.value)}></input>
                            <span class="icon is-small is-left">
                                <i class="fas fa-lock"></i>
                            </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control">
                        <button class="button is-dark" onClick={submitForm}>
                            S'inscrire
                        </button>
                    </p>
                </div>
            </form>
        </div>
    );
}
export default Register