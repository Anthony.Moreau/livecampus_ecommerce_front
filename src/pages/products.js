import { useContext, useState, useEffect } from "react"
import { useNavigate} from "react-router-dom";
import { getAllProducts } from "../api/products"
import { EcommerceContext } from "../contexte/ecommerce";

import Card from '../components/card';

function Home() {

    const [cards, setCards] = useState([])
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getAllProducts().then(data => {
            setCards(data)
            console.log(data)
        })
    }, [])

    return (
        <div className='productpage'>
            <p className='title is-4' style={{textAlign:'center'}}>Liste des produits</p>
            <ul className='gridview'>
                {cards.map(card => (<li class='gridelem'><Card key={card.id} id={card.id} photo={card.photo} nom={card.nom} description={card.description} prix={card.prix}></Card> </li>))}
            </ul>
        </div>
    );
}
export default Home