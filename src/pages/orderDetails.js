import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getOrderLines } from "../api/order";

function OrderDetail() {
    const { id } = useParams();
    const [orderLines, setOrderLines] = useState([])
    const [date, setDate] = useState("")
    const [statut, setStatut] = useState("")

    useEffect(() => {

        getOrderLines(id).then(data => {
            console.log(data);
            setDate(data[0].date)
            setStatut(data[0].statut)
            setOrderLines(data)
        })
    }, [])
    return (
        <div className='app'>
            <div className='command-info'>
                <p className='title is-4'>Date : <span style={{color:'blue'}}>{date}</span></p>
                <p className='title is-4'>Statut : <span style={{color:'blue'}}>{statut}</span></p>
            </div>
            <hr style={{width: '2px'}}/>

            <div className='command-detail'>
                <ul>
                    {orderLines.map(item => (
                        <>
                            <li>
                                <span className='title is-6'>{item.qte} x {item.nom}</span>
                                <br />
                                <span className='is-italic'>{item.description}</span>
                                <br />
                                <span className='title is-6'>{item.prix}</span>
                                <br />

                            </li>
                            <hr />
                        </>
                    ))}
                </ul>
            </div>

        </div>
    );
}
export { OrderDetail }
