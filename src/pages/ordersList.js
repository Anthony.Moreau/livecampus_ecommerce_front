import '../App.css';
import React, { useContext, useEffect, useState } from 'react';
import { Text } from 'react-dom'
import 'bulma/css/bulma.min.css'
import { EcommerceContext } from '../contexte/ecommerce';
import { Eye, Pencil, TrashFill } from 'react-bootstrap-icons'
import { Link } from 'react-router-dom';
import { useNavigate, useParams  } from "react-router-dom";


import { getAllOrdersByUser, getAllOrders, getOrder, editOrder, deleteOrder } from "../api/order";

function OrderList() {
    const { token, mail, admin } = useContext(EcommerceContext);
    const [orders, setOrders] = useState([])
    const [keys, setKeys] = useState([])
    
    useEffect(() => {
        if(admin === true){
            getAllOrders().then(data => {
                setOrders(data);
                setKeys(Object.keys(data[0]));
            })
        }
        else{
            
            getAllOrdersByUser(mail).then(data => {
                console.log(mail)
                console.log(data);
                setOrders(data);
                setKeys(Object.keys(data[0]));
            })
        }
    }, [])

    return (
        <div className='order_details'>
            <h1 className='title is-4'>Liste des commandes</h1>
            <table className="table">
                <thead>
                    <tr>
                        {keys.map(key => (
                            <th><abbr title={key}>{key}</abbr></th>
                        ))}
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {orders.map(order => (
                        <tr>
                            {keys.map(key => (
                                <td>{order[key]}</td>
                            ))}
                            <td className="is-flex is-flex-direction-column">
                                <Link to={`/orders/${order['id']}`} className="button is-info my-1"><Eye /></Link>
                            </td>
                            {(admin === true) ?
                                <>
                                    <td className="is-flex is-flex-direction-column">
                                        <Link to={`/orders/edit/${order['id']}`} className="button is-danger my-1"><Pencil /></Link>
                                        <Link to={`/orders/delete/${order['id']}`} className="button is-danger my-1"><TrashFill /></Link>
                                    </td>
                                </>
                                :
                                <>
                                </>
                            }
                        </tr>
                    ))}

                </tbody>
            </table>
        </div>
    );
}

function OrderEdit() {
    const { token } = useContext(EcommerceContext);

    const { id } = useParams();
    const [statut, setStatut] = useState("")
    useEffect(() => {
        // write your code here, it's like componentWillMount
        getOrder(id).then(data => {
            setStatut(data.statut)
        })
    }, [])


    const navigate = useNavigate()
    const statusChanged = (e)=> {
        setStatut(e.target.value)
    }

    const submitForm = (e) => {
        e.preventDefault();
        editOrder(id, statut, token).then(data => {
            alert(data.mess)
            navigate('/orders')
        })
    }
    return (
        <div className='app'>
            <h1 className='title is-4'>Modifier une commande</h1>
            <div className='auth-box'>
                <form action="/" method='post' className='box form-box' onSubmit={submitForm}>
                    <h1 className='title is-4'>Edit</h1>
                    <select value={statut} onChange={statusChanged}>
                        <option value="Validé">Validé</option>
                        <option value="Payé">Payé</option>
                        <option value="Livré">Livré</option>
                        <option value="Annulé">Annulé</option>
                    </select>
                    <button className="button is-primary is-light">Editer le statut de la commande</button>
                </form>
            </div>

        </div >
    );
}
function OrderDelete() {
    const { token } = useContext(EcommerceContext);

    const navigate = useNavigate()
    const { id } = useParams();

    useEffect(() => {
        deleteOrder(id, token).then(data => {
            alert(data.mess)
            navigate('/orders')
        })
    }, [])
}


export { OrderList, OrderDelete, OrderEdit }
