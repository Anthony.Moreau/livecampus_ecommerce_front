import Card from '../components/card';

function Home() {
    return (
        <section className="hero is-dark is-fullheight-with-navbar">
            <div className="hero-body">
                <div className="container has-text-centered">
                    <h1 className="title">Bienvenue sur mon projet E-Commerce</h1>
                    <h2 className="subtitle">Un projet réalisé dans le cadre d'un devoir LiveCampus</h2>
                </div>
            </div>
        </section>
    );
}
export default Home