import '../App.css';
import React, { useContext, useEffect, useState } from 'react';
import { deleteUser, editUser, getAllUsers } from '../api/user';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { PatchCheck, PatchCheckFill, TrashFill } from 'react-bootstrap-icons'
import { EcommerceContext } from '../contexte/ecommerce';



function UserParam() {
    const [users, setUsers] = useState([])
    const [keys, setKeys] = useState([])

    useEffect(() => {
        // write your code here, it's like componentWillMount
        getAllUsers().then(data => {
            setUsers(data)
            setKeys(Object.keys(data[0]))
        })
    }, [])
    return (
        <div className='user_param'>
            <h1 className='title is-4'>Liste des utilisateurs</h1>
            <table className="table">
                <thead>
                    <tr>
                        {keys.map(key => (
                            <th><abbr title={key}>{key}</abbr></th>
                        ))}
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {users.map(user => (
                        <tr>
                            {keys.map(key => (
                                <td>{key === 'niveau' ?
                                    (user[key] === 1 ?
                                        <Link to={`/usersParam/edit/${user['id']}?niveau=${user['niveau']}`} ><PatchCheckFill color='green' /> Admin</Link>
                                        : <Link to={`/usersParam/edit/${user['id']}?niveau=${user['niveau']}`} ><PatchCheck color='green' /> Utilisateur</Link>)
                                    : user[key]}</td>
                            ))}
                            <td className="is-flex is-flex-direction-column">
                                <Link to={`/usersParam/delete/${user['id']}`} className="button is-danger my-1"><TrashFill /></Link>
                            </td>
                        </tr>
                    ))}

                </tbody>
            </table>
        </div>
    );
}
function UserDelete() {
    const { token } = useContext(EcommerceContext);

    const navigate = useNavigate()
    const { id } = useParams();

    useEffect(() => {
        deleteUser(id, token).then(data => {
            alert(data.mess)
            navigate('/usersParam')
        })
    }, [])
}
function UserEdit() {
    const { token } = useContext(EcommerceContext);
    const queryParameters = new URLSearchParams(window.location.search)
    const niveau = queryParameters.get('niveau')

    const navigate = useNavigate()
    const { id } = useParams();

    useEffect(() => {
        editUser(id, niveau == 1 ? 0 : 1, token).then(data => {
            alert(data.mess)
            navigate('/usersParam')
        })
    }, [])
}
export {UserParam, UserDelete, UserEdit};
